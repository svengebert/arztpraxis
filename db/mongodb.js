var MongoClient = require('mongodb').MongoClient,
url = "mongodb://localhost:27017/arztdb";

module.exports = {

    insertOne(collection, object) {
        MongoClient.connect(url, function (err, db) {
            if (err) throw err;
        
            var dbo = db.db("arztdb");
            dbo.collection(collection).insertOne(object, function (err, res) {
                if (err) throw err;
                db.close();
            });
        });
    },

    insertMany(collection, data) {
        MongoClient.connect(url, function (err, db) {
            if (err) throw err;
        
            var dbo = db.db("arztdb");
            dbo.collection(collection).insertMany(data, function (err, res) {
                if (err) throw err;
                db.close();
            });
        });
    },

    count(collection) {
        return new Promise((resolve, reject) => {
            MongoClient.connect(url, function (err, db) {
                if (err) throw err;
                var dbo = db.db("arztdb");

                dbo.collection(collection).countDocuments({}, function (err, res) {
                    if (err) throw err;
                    db.close();
                    if (res) resolve(res);
                    else resolve(null);
                });
            });
        });
    },

    check(nickname) {
        return new Promise((resolve, reject) => {
            MongoClient.connect(url, function (err, db) {
                if (err) throw err;
                var dbo = db.db("arztdb");

                dbo.collection('user').countDocuments({benutzername: nickname}, function (err, res) {
                    if (err) throw err;
                    db.close();

                    if (res) resolve(res);
                    else resolve(null);
                });
            });
        });
    },

    findUser(nickname, pw, role) {
        return new Promise((resolve, reject) => {
            MongoClient.connect(url, function (err, db) {
                if (err) throw err;
                var dbo = db.db("arztdb");

                dbo.collection('user').findOne( { benutzername: nickname.toString(), passwort: pw.toString(), rolle: role.toString() }, function (err, res) {
                    if (err) throw err;
                    db.close();
                    if (res) resolve(res);
                    else resolve(null);
                });
            });
        });
    },

    deleteCookie(cookie) {
        return new Promise((resolve, reject) => {
            MongoClient.connect(url, function (err, db) {
                if (err) throw err;
            
                if(cookie == null)
                    return resolve(null);

                var dbo = db.db("arztdb");
                dbo.collection('cookies').deleteOne( { cookie: cookie.toString() }, function (err, res) {
                    if (err) throw err;
                    db.close();
                    if (res) resolve(res);
                    else resolve(null);
                });
            });
        });
    },

    getUserId(cookie) {
        return new Promise((resolve, reject) => {
            MongoClient.connect(url, function (err, db) {
                if (err) throw err;
            
                if(cookie == null)
                    return resolve(null);

                var dbo = db.db("arztdb");
                dbo.collection('cookies').findOne( { cookie: cookie.toString() }, function (err, res) {
                    if (err) throw err;
    
                    db.close();
                    if (res) resolve(res.user_id);
                    else resolve(null);
                });
            });
        });
    },

    getUser(userId) {
        return new Promise((resolve, reject) => {
            MongoClient.connect(url, function (err, db) {
                if (err) throw err;
                var dbo = db.db("arztdb");

                dbo.collection('user').findOne( { _id: userId }, function (err, res) {
                    if (err) throw err;
                    db.close();
                    if (res) resolve(res);
                    else resolve(null);
                });
            });
        });
    },

    getAppointments(userId, role) {
        return new Promise((resolve, reject) => {
            MongoClient.connect(url, function (err, db) {
                if (err) throw err;

                var newDate = new Date();
                var currentDate = newDate.getTime();
                   
                var dbo = db.db("arztdb");
                if (role == 'patient') {
                    dbo.collection('termin').aggregate([
                        { $lookup:
                            {
                                from: 'user',
                                localField: 'arzt_id',
                                foreignField: '_id',
                                as: 'arzt'
                            }
                        },
                        { $sort:
                            {
                                date: 1
                            }
                        },
                        { $match:
                            {
                                date: {$gte: currentDate},
                                patient_id: userId
                            }
                        }
                    ]).toArray( function (err, res) {
                        if (err) throw err;
                        db.close();
                        if (res) resolve(res);
                        else resolve(null);
                    });
                }
                else if (role == 'arzt') {
                    dbo.collection('termin').aggregate([
                        { $lookup:
                            {
                                from: 'user',
                                localField: 'patient_id',
                                foreignField: '_id',
                                as: 'patient'
                            }
                        },
                        { $sort:
                            {
                                date: 1
                            }
                        },
                        { $match:
                            {
                                date: {$gte: currentDate},
                                arzt_id: userId
                            }
                        }
                    ]).toArray( function (err, res) {
                        if (err) throw err;
                        db.close();
                        if (res) resolve(res);
                        else resolve(null);
                    });
                }

                else if (role == 'angehoeriger') {
                    dbo.collection('termin').aggregate([
                        { $lookup:
                            {
                                from: 'user',
                                localField: 'arzt_id',
                                foreignField: '_id',
                                as: 'arzt'
                            }
                        },
                        { $sort:
                            {
                                date: 1
                            }
                        },
                        {
                            $match: 
                            {
                                patient_id: userId,
                                date: {$gte: currentDate}
                            }
                        }
                    ]).toArray( function (err, res) {
                        if (err) throw err;
                        db.close();
                        if (res) resolve(res);
                        else resolve(null);
                    });
                }
            });
        });
    },

    getMeasurements(userId) {
        return new Promise((resolve, reject) => {
            MongoClient.connect(url, function (err, db) {
                if (err) throw err;
                var dbo = db.db("arztdb");

                dbo.collection('messwert').aggregate([
                    { $sort:
                        {
                            date: -1
                        }
                    },
                    {
                        $match: 
                        {
                            patient_id: userId 
                        }
                    }                                  
                ]).toArray( function (err, res) {
                    if (err) throw err;
                    db.close();
                    if (res) resolve(res);
                    else resolve(null);
                });
            });
        });
    },

    getPatients(userId, role) {
        return new Promise((resolve, reject) => {
            MongoClient.connect(url, function (err, db) {
                if (err) throw err;
                var dbo = db.db("arztdb");
                
                if (role == 'arzt') {
                    dbo.collection('angehoerige').aggregate([
                        { $lookup:
                            {
                                from: 'user',
                                localField: 'patient_id',
                                foreignField: '_id',
                                as: 'patient'
                            }
                        },
                        {
                            $lookup: 
                            {
                                from: 'user',
                                localField: 'angehoeriger_id',
                                foreignField: '_id',
                                as: 'angehoeriger'
                            }
                        }
                    ]).toArray( function (err, res) {
                        if (err) throw err;
                        db.close();
                        resolve(res);
                    });
                }

                else if (role == 'angehoeriger') {
                    dbo.collection('angehoerige').aggregate([
                        { $lookup:
                            {
                                from: 'user',
                                localField: 'patient_id',
                                foreignField: '_id',
                                as: 'patient'
                            }
                        },
                        {
                            $match: 
                            {
                                angehoeriger_id: userId
                            }
                        }
                    ]).toArray( function (err, res) {
                        if (err) throw err;
                        db.close();
                        resolve(res);
                    });
                }
            });
        });
    },

    getPatient(selectedId, userId, userRole) {
        return new Promise((resolve, reject) => {
            MongoClient.connect(url, function (err, db) {
                if (err) throw err;
                var dbo = db.db("arztdb");
                
                if (userRole == 'arzt') {
                    dbo.collection('angehoerige').aggregate([
                        { $lookup:
                            {
                                from: 'user',
                                localField: 'patient_id',
                                foreignField: '_id',
                                as: 'patient'
                            }
                        },
                        {
                            $lookup: 
                            {
                                from: 'user',
                                localField: 'angehoeriger_id',
                                foreignField: '_id',
                                as: 'angehoeriger'
                            }
                        },
                        {
                            $match: 
                            {
                                patient_id: selectedId
                            }
                        }
                    ]).toArray( function (err, res) {
                        if (err) throw err;
                        db.close();
                        if (res) resolve(res);
                        else resolve(null);
                    });
                }

                else if (userRole == 'angehoeriger') {
                    dbo.collection('angehoerige').aggregate([
                        { $lookup:
                            {
                                from: 'user',
                                localField: 'patient_id',
                                foreignField: '_id',
                                as: 'patient'
                            }
                        },
                        {
                            $match: 
                            {
                                angehoeriger_id: userId,
                                patient_id: selectedId
                            }
                        }
                    ]).toArray( function (err, res) {
                        if (err) throw err;
                        db.close();
                        if (res) resolve(res);
                        else resolve(null);
                    });
                }
            });
        });
    },

    getSelectedId(user) {
        return new Promise((resolve, reject) => {
            MongoClient.connect(url, function (err, db) {
                if (err) throw err;
                var dbo = db.db("arztdb");

                dbo.collection('cookies').findOne( { user_id: user._id },  function (err, res) {
                    if (err) throw err;
                    db.close();
                    if (res) resolve(res.selection);
                    else resolve(null);
                });
            });
        });
    },

    getAppointment(app_id) {
        return new Promise((resolve, reject) => {
            MongoClient.connect(url, function (err, db) {
                if (err) throw err;
                var dbo = db.db("arztdb");

                dbo.collection('termin').findOne( { _id: app_id },  function (err, res) {
                    if (err) throw err;
                    db.close();
                    if (res) resolve(res);
                    else resolve(null);
                });
            });
        });
    },

    getSelectedAppointment(user) {
        return new Promise((resolve, reject) => {
            MongoClient.connect(url, function (err, db) {
                if (err) throw err;
                var dbo = db.db("arztdb");

                dbo.collection('cookies').findOne( { user_id: user._id },  function (err, res) {
                    if (err) throw err;
                    db.close();
                    if (res) resolve(res.selectedApp);
                    else resolve(null);
                });
            });
        });
    },

    updateCookies(userId, cookie, selection) {
        return new Promise((resolve, reject) => {
            MongoClient.connect(url, function (err, db) {
                if (err) throw err;
                var dbo = db.db("arztdb");

                dbo.collection('cookies').updateOne( { user_id: userId },{ $set: {user_id: userId, cookie: cookie, selection: selection }}, function (err, res) {
                    if (err) throw err;
                    db.close();
                    if (res) resolve(res);
                    else resolve(null);
                });
            });
        });
    },

    getAerzte() {
        return new Promise((resolve, reject) => {
            MongoClient.connect(url, function (err, db) {
                if (err) throw err;
                var dbo = db.db("arztdb");

                dbo.collection('user').find( { rolle: 'arzt' } ).toArray( function (err, res) {
                    if (err) throw err;
                    db.close();
                    if (res) resolve(res);
                    else resolve(null);
                });
            });
        });
    },

    deleteMeasurement(id) {
        return new Promise((resolve, reject) => {
            MongoClient.connect(url, function (err, db) {
                if (err) throw err;
                var dbo = db.db("arztdb");

                dbo.collection('messwert').deleteOne( { _id: id }, function (err, res) {
                    if (err) throw err;
                    db.close();
                    if (res) resolve(res);
                    else resolve(null);;
                });
            });
        });
    },

    deleteAppointment(id) {
        return new Promise((resolve, reject) => {
            MongoClient.connect(url, function (err, db) {
                if (err) throw err;
                var dbo = db.db("arztdb");

                dbo.collection('termin').deleteOne( { _id: id }, function (err, res) {
                    if (err) throw err;
                    db.close();
                    if (res) resolve(res);
                    else resolve(null);
                });
            });
        });
    },
    
    getUserStatus(id) {
        return new Promise((resolve, reject) => {
            MongoClient.connect(url, function (err, db) {
                if (err) throw err;
                var dbo = db.db("arztdb");

                dbo.collection('angehoerige').findOne( { patient_id: id }, function (err, res) {
                    if (err) throw err;
                    db.close();
                    if (res) resolve(res);
                    else resolve(null);
                });
            });
        });
    },

    findAn(id) {
        return new Promise((resolve, reject) => {
            MongoClient.connect(url, function (err, db) {
                if (err) throw err;
                var dbo = db.db("arztdb");

                dbo.collection('user').findOne( { _id: id, rolle: 'angehoeriger' }, function (err, res) {
                    if (err) throw err;
                    db.close();
                    if (res) resolve(res);
                    else resolve(null);
                });
            });
        });
    },

    updateAn(patient_id, angehoeriger_id) {
        return new Promise((resolve, reject) => {
            MongoClient.connect(url, function (err, db) {
                if (err) throw err;
                var dbo = db.db("arztdb");

                dbo.collection('angehoerige').updateOne( { patient_id: patient_id }, { $set: {patient_id: patient_id, angehoeriger_id: angehoeriger_id}}, function (err, res) {
                    if (err) throw err;
                    db.close();
                    if (res) resolve(res);
                    else resolve(null);
                });
            });
        });
    },

    updateAppointment(id, patient_id, arzt_id, behandlung, datum, uhrzeit, date) {
        return new Promise((resolve, reject) => {
            MongoClient.connect(url, function (err, db) {
                if (err) throw err;
                var dbo = db.db("arztdb");

                dbo.collection('termin').updateOne( { _id: id }, { $set: {patient_id: patient_id, arzt_id: arzt_id, behandlung: behandlung, datum: datum, uhrzeit: uhrzeit, date: date }}, function (err, res) {
                    if (err) throw err;
                    db.close();
                    if (res) resolve(res);
                    else resolve(null);
                });
            });
        });
    },

    updateSelectedAppointment(user_id, app_id) {
        return new Promise((resolve, reject) => {
            MongoClient.connect(url, function (err, db) {
                if (err) throw err;
                var dbo = db.db("arztdb");

                dbo.collection('cookies').updateOne( { user_id: user_id }, { $set: { selectedApp: app_id }}, function (err, res) {
                    if (err) throw err;
                    db.close();
                    if (res) resolve(res);
                    else resolve(null);
                });
            });
        });
    }
}