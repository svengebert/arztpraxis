function changeMask() {
	var e = document.getElementById("case");
	var opt = e.options[e.selectedIndex].value;
	var e2;
	console.log(opt);
	switch(opt) {
		case "Blutdruck":
			e2 = document.getElementById("input_p_sys");
			e2.required = true;
			e2.type = "text";
			e2 = document.getElementById("input_p_dia");
			e2.required = true;
			e2.type = "text";
			e2 = document.getElementById("input_p_pulse");
			e2.required = true;
			e2.type = "text";
			e2 = document.getElementById("input_p_chol");
			e2.required = false;
			e2.type = "hidden";
			e2 = document.getElementById("input_p_bs");
			e2.required = false;
			e2.type = "hidden";
			e2 = document.getElementById("input_p_temp");
			e2.required = false;
			e2.type = "hidden";
			break;
		case "Cholesterin":
			e2 = document.getElementById("input_p_sys");
			e2.required = false;
			e2.type = "hidden";
			e2 = document.getElementById("input_p_dia");
			e2.required = false;
			e2.type = "hidden";
			e2 = document.getElementById("input_p_pulse");
			e2.required = false;
			e2.type = "hidden";
			e2 = document.getElementById("input_p_chol");
			e2.required = true;
			e2.type = "text";
			e2 = document.getElementById("input_p_bs");
			e2.required = false;
			e2.type = "hidden";
			e2 = document.getElementById("input_p_temp");
			e2.required = false;
			e2.type = "hidden";
			break;
		case "Blutzucker":
			e2 = document.getElementById("input_p_sys");
			e2.required = false;
			e2.type = "hidden";
			e2 = document.getElementById("input_p_dia");
			e2.required = false;
			e2.type = "hidden";
			e2 = document.getElementById("input_p_pulse");
			e2.required = false;
			e2.type = "hidden";
			e2 = document.getElementById("input_p_chol");
			e2.required = false;
			e2.type = "hidden";
			e2 = document.getElementById("input_p_bs");
			e2.required = true;
			e2.type = "text";
			e2 = document.getElementById("input_p_temp");
			e2.required = false;
			e2.type = "hidden";
			break;
		case "Temperatur":
			e2 = document.getElementById("input_p_sys");
			e2.required = false;
			e2.type = "hidden";
			e2 = document.getElementById("input_p_dia");
			e2.required = false;
			e2.type = "hidden";
			e2 = document.getElementById("input_p_pulse");
			e2.required = false;
			e2.type = "hidden";
			e2 = document.getElementById("input_p_chol");
			e2.required = false;
			e2.type = "hidden";
			e2 = document.getElementById("input_p_bs");
			e2.required = false;
			e2.type = "hidden";
			e2 = document.getElementById("input_p_temp");
			e2.required = true;
			e2.type = "text";
			break;
	}
}

function clickableTable(table) {
	var table = document.getElementById(table);
	var cells = table.getElementsByTagName('tr');

	for (var i = 0; i < cells.length; i++) {

		// Take each cell
		var cell = cells[i];
		// do something on onclick event for cell
		cell.onclick = function () {

			location.reload();

			var xhr = new XMLHttpRequest();
			xhr.open( 'POST', `http://localhost:8081/select`, true ); // false for synchronous request
			xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
			xhr.send( `id=${this.cells[0].parentNode.getAttribute('id')}` );
		}
	}
}

function editAble(table) {
	var table = document.getElementById(table);
	var buttons = table.getElementsByClassName('edit-button');

	console.log(buttons);

	for (var i = 0; i < buttons.length; i++) {

		// Take each cell
		var button = buttons[i];
		// do something on onclick event for cell
		button.onclick = function () {
		
			location.reload();

			var path = window.location.pathname;
			var page = path.split("/").pop();
			page = page.split(".")[0];

			var td = this.parentNode;
			var tr = td.parentNode;

			var xhr = new XMLHttpRequest();
			xhr.open( 'POST', `http://localhost:8081/edit`, true ); // false for synchronous request
			xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
			xhr.send( `id=${tr.getAttribute('id')}&kind=${tr.getAttribute('name')}&page=${page}`);
		}
	}
}

function deleteAble(table) {
	var table = document.getElementById(table);
	var buttons = table.getElementsByClassName('delete-button');

	for (var i = 0; i < buttons.length; i++) {
		
		// Take each cell
		var button = buttons[i];
		// do something on onclick event for cell
		button.onclick = function () {
		
			location.reload();

			var path = window.location.pathname;
			var page = path.split("/").pop();
			page = page.split(".")[0];

			var td = this.parentNode;
			var tr = td.parentNode;

			console.log(tr);

			var xhr = new XMLHttpRequest();
			xhr.open( 'POST', `http://localhost:8081/delete`, true ); // false for synchronous request
			xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
			xhr.send( `id=${tr.getAttribute('id')}&kind=${tr.getAttribute('name')}&page=${page}`);
		}
	}
}
