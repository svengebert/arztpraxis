const http = require('http'),
fs = require('fs'),
mongodb = require('mongodb');

const path = require('path');
const { type } = require('os');
const handlebars = require('handlebars');
const { parse } = require('querystring');

const db = require('./db/mongodb');

async function handleRequest(request, response) {

    if (request.method === 'GET') {

        if (request.url.match(/index.html/)) {
            serveIndex(request, response);
        }
        else if (request.url.match(/.html/g)) {
            serveHTML(request, response);
        }
        else if (request.url.match(/.css/g)) {
            serveCSS(request, response);
        }
        else if (request.url.match(/.js/g)) {
            serveJS(request, response);
        }
        else if (request.url.match(/.png/g) || request.url.match(/.jpg/g) || (request.url.match(/.ico/g))) {
            serveIMG(request, response);
        }
        else if (request.url.match(/^\/$/)) {

            var cookie = request.headers.cookie;
			var userId = await db.getUserId(cookie).catch(error => console.error(error));;
            
            if (userId == null) {
	            serveIndex(request, response);
				return;
			} else {
                var user = await db.getUser(userId).catch(error => console.error(error));

                if (user.rolle == 'patient') {
                    response.setHeader('Location', '/pa_messdaten.html');
                    serveView(request, response, 'pa_messdaten');
                }
                else if (user.rolle == 'arzt') {
                    response.setHeader('Location', '/ar_patienten.html');
                    serveView(request, response, 'ar_patienten');
                }
                else if (user.rolle == 'angehoeriger') {
                    response.setHeader('Location', '/an_angehoerige.html');
                    serveView(request, response, 'an_angehoerige');
                }
            }
        }
        else {
            console.log(`Request(${path.basename(request.url)}): Can't handle file - Invalid Extension`);
            return;
        }
        console.log(`GET (${request.url}): Antwort erfolgreich gesendet.`);

    } else if (request.method === 'POST') {

        // REGISTER
        if (request.url.match(/register/g)) {

            let numOfUsers = 0;
            let newUser;

            newUser = await collectRequestData(request);

            // Nutzername abgleichen
            let nameTaken = await db.check(newUser.nickname);
            if (nameTaken) {
                console.log(`Benutzername bereits vergeben.`);
                serveIndex(request, response);
            } 
            
            // Registieren des neuen Nutzers
            else {

                // Anzahl der registrierten Nutzer bestimmen
                numOfUsers = await db.count('user');

                var geschlecht;
                if (newUser.gender == undefined)
                    geschlecht = "männlich";
                else
                    geschlecht = "weiblich";

                db.insertOne("user", {_id: (numOfUsers + 1), benutzername: newUser.nickname, vorname: newUser.prename, nachname: newUser.name, 
                    geburtsdatum: newUser.birthdate, geschlecht: geschlecht, telefon: newUser.phone, land: newUser.city, plz: newUser.plz, ort: newUser.city, 
                    strasse: newUser.street, email: newUser.mail, passwort: newUser.pw, rolle: newUser.role});

                if (newUser.role == 'patient')
                    db.insertOne("angehoerige", {patient_id: (numOfUsers + 1), angehoeriger_id: null});

                serveIndex(request, response);
            }
        } 

        // LOGIN
        else if (request.url.match(/login/g)) {
           
            let user;
            let data;

            data = await collectRequestData(request);

            // pruefe ob ein Nutzer mit dieser Kombination exisistiert
            user = await db.findUser(data.nickname, data.password, data.role);
            if (user == null) {
                console.log(`Benutzername, Passwort oder Rolle falsch.`);
                serveIndex(request, response);
            }

            // erzeuge cookie und abspeichern von userID und cookie in DB
            else {
                console.log(`Login erfolgreich.`);

                var cookie = user._id.toString();
                cookie += generateCookie();

                db.insertOne('cookies', {user_id: user._id, cookie: cookie, selection: 0, selectedApp: 0});
                
                response.setHeader('Set-Cookie', cookie);
            
                if (user.rolle == 'patient') {
                    response.setHeader('Location', '/pa_termine.html');
                    serveView(request, response, 'pa_termine');
                }
                else if (user.rolle == 'arzt') {
                    response.setHeader('Location', '/ar_patienten.html');
                    serveView(request, response, 'ar_patienten');
                }
                else if (user.rolle == 'angehoeriger') {
                    response.setHeader('Location', '/an_angehoerige.html');
                    serveView(request, response, 'an_angehoerige');
                }
            }
        }
        
        // LOGOUT
        else if (request.url.match(/logout/g)) {

            cookie = request.headers.cookie;

            // eintrag aus userID/cookie tabelle entfernen
            await db.deleteCookie(cookie);

            // cookie entfernen
            response.setHeader('Set-Cookie', 0);
            response.setHeader('Location', '/index.html');
            response.statusCode = 303;
           
            console.log(`Logout.`);

            serveIndex(request, response);
        } 

        // SELECT
        else if (request.url.match(/select/g)) {

            var cookie = request.headers.cookie;
            var userId = await db.getUserId(cookie);
            var user = await db.getUser(userId);

            data = await collectRequestData(request);
            selectedId = data.id;

            await db.updateCookies(user._id, cookie, Number(selectedId));

            response.statusCode = 303;

            if (user.rolle == 'arzt') {
                response.setHeader('Location', '/ar_patienten.html');
                serveView(request, response, 'ar_patienten');
            }
            else if (user.rolle == 'angehoeriger') {
                response.setHeader('Location', '/an_angehoerige.html');
                serveView(request, response, 'an_angehoerige');
            }
        }

        // EDIT
        else if (request.url.match(/edit/g)) {

            var cookie = request.headers.cookie;
            var userId = await db.getUserId(cookie);
            var user = await db.getUser(userId);
            var data = await collectRequestData(request);

            var id = new mongodb.ObjectID(data.id);

            await db.updateSelectedAppointment(user._id, id);

            response.statusCode = 303;
            response.setHeader('Location', `/${data.page}.html`);
            serveView(request, response, `${data.page}`);
        }

        // DELETE
        else if (request.url.match(/delete/g)) {

            var cookie = request.headers.cookie;
            var userId = await db.getUserId(cookie);
            var user = await db.getUser(userId);
            var data = await collectRequestData(request);
        
            var id = new mongodb.ObjectID(data.id);

            if (data.kind == 'messwert')
                await db.deleteMeasurement(id);
            else if (data.kind == 'termin')
                await db.deleteAppointment(id);

            response.statusCode = 303;
            response.setHeader('Location', `/${data.page}.html`);
            serveView(request, response, `${data.page}`);
        }

        // CHANGEAPP
        else if (request.url.match(/changeapp/g)) {

            var cookie = request.headers.cookie;
            var userId = await db.getUserId(cookie);
            var user = await db.getUser(userId);
            var data = await collectRequestData(request);

            var date = Date.parse(`${data.datum}T${data.uhrzeit}:00`);

            // Wenn ein Termin ausgewaehlt wurde
            if (data.termin_id != '') {
                var id = new mongodb.ObjectID(data.termin_id);
                await db.updateAppointment(id, Number(data.patient_id), Number(data.doctor), data.case, data.datum, data.uhrzeit, date);
            }

            response.statusCode = 303;
            response.setHeader('Location', `/an_angehoerige.html`);
            serveView(request, response, `an_angehoerige`);
        }

        // ANGEHOERIGER
        else if (request.url.match(/angehoeriger/g)) {

            var cookie = request.headers.cookie;
            var userId = await db.getUserId(cookie);
            var user = await db.getUser(userId);
            var data = await collectRequestData(request);

            var userStatusObject = await db.getUserStatus(userId);
            if (userStatusObject.angehoeriger_id == null) {

                // Patient hat einen Eintrag ist aber mit keinem Angehoerigen verlinkt
                var angehoeriger = await db.findAn(Number(data.angehoeriger));
                if (angehoeriger != null) {
                    await db.updateAn(Number(data.userid), Number(data.angehoeriger));
                }
            } else {
                // Patient hat noch keinen Eintrag in der Tabelle Angehoerige (nur bei DB-Fehler)
                db.insertOne('angehoerige', {patient_id: Number(data.userid), angehoeriger_id: Number(data.angehoeriger)})
            }

            response.statusCode = 303;
            response.setHeader('Location', `/pa_termine.html`);
            serveView(request, response, `pa_termine`);
        }

        // MESSDATEN
        else if (request.url.match(/messdaten/g)) {

            console.log("DATTEEEEN");

			cookie = request.headers.cookie;
			userId = await db.getUserId(cookie);
            
            if(userId == null) {
	            serveIndex(request, response);
				return;
			}

            userData = await db.getUser(userId);
			data = await collectRequestData(request);
                        
            console.log(`POST Neue messdaten.`);
            var datetime = new Date();
            var value;

            switch(data.case) {
                case "Blutdruck":
                    value = `SYS: ${data.input_p_sys} DIA: ${data.input_p_dia} Puls: ${data.input_p_pulse}`;
                    break;
                case "Cholesterin":
                    value = `${data.input_p_chol} mg/dl`;
                    break;
                case "Blutzucker":
                    value = `${data.input_p_bs} mg/dl`;
                    break;
                case "Temperatur":
                    value = `${data.input_p_temp} °C`;
                    break;
            }

            db.insertOne("messwert", {patient_id: Number(data.input_p_uid), bezeichnung: data.case, werte: value, 
                datum: datetime.toISOString().slice(0,10), uhrzeit: datetime.toISOString().slice(11,19), date: datetime.getTime()});

            response.statusCode = 303;

            if (userData.rolle == 'patient') {
                response.setHeader('Location', '/pa_messdaten.html');
                serveView(request, response, 'pa_messdaten');
            }
            else if (userData.rolle == 'arzt') {
                response.setHeader('Location', '/ar_patienten.html');
                serveView(request, response, 'ar_patienten');
            }
            else if (userData.rolle == 'angehoeriger') {
                response.setHeader('Location', '/an_angehoerige.html');
                serveView(request, response, 'an_angehoerige');
            }
        } 
        
        // TERMIN
        else if (request.url.match(/termin/g)) {
            
            cookie = request.headers.cookie;

            userId = await db.getUserId(cookie);
			if (userId == null) {
	            serveIndex(request, response);
				return;
			}

            userData = await db.getUser(userId);
			if (userData.rolle != 'patient') {
	            serveIndex(request, response);
				return;
			}

            data = await collectRequestData(request);
            console.log(`POST Neuer termin.`);

            var date = Date.parse(`${data.datum}T${data.uhrzeit}:00`);

            db.insertOne("termin", {patient_id: Number(data.patient), behandlung: data.case, arzt_id: Number(data.doctor), datum: data.datum, uhrzeit: data.uhrzeit, date: date});

            response.statusCode = 303;
            response.setHeader('Location', '/pa_termine.html');
            serveView(request, response, 'pa_termine');
		}
    }
}

async function getRoleData(request) {

    let webData = null;

    var cookie = request.headers.cookie;
    var userId = await db.getUserId(cookie);
    // TODO: wenn nicht vorhanden zurück zu startseite
    var user = await db.getUser(userId);

    if (user == null)
        return null;

    // PATIENT
    if (user.rolle == 'patient') {

        var t_list = await db.getAppointments(user._id, user.rolle);
        var m_list = await db.getMeasurements(user._id);
        var a_list = await db.getAerzte();

        var nextApmnt = t_list[0];

        webData = {userData: user, t_list: t_list, m_list: m_list, nextApmnt: nextApmnt, a_list: a_list};
    }

    // ARZT
    else if (user.rolle == 'arzt') {

        let p_list;
        let t_list;
        let m_list;
        let selected;

        t_list = await db.getAppointments(user._id, user.rolle).catch(error => console.error(error));

        p_list = await db.getPatients(user._id, user.rolle).catch(error => console.error(error));
        if (p_list.length <= 0)
            return webData = {userData: user, m_list: null, t_list: t_list, p_list: null, selected: null};

        console.log(p_list);

        var selectedId = await db.getSelectedId(user).catch(error => console.error(error));
        if (selectedId == undefined)
            selectedId = 0;

        if (selectedId <= 0) {
            var selectedP = p_list[0].patient[0];
            var selectedA = p_list[0].angehoeriger[0];
            selected = {patient: selectedP, angehoeriger: selectedA};

        } else {
            var tmpP = await db.getPatient(selectedId, user._id, user.rolle);
            var selectedP = tmpP[0].patient[0];
            var selectedA = tmpP[0].angehoeriger[0];
            selected = {patient: selectedP, angehoeriger: selectedA};
        }
    
        m_list = await db.getMeasurements(selected.patient._id);

        webData = {userData: user, m_list: m_list, t_list: t_list, p_list: p_list, selected: selected};
    }

    // ANGEHOERIGER
    else if (user.rolle == 'angehoeriger') {

        let p_list;
        let t_list;
        let selected;
        let selectedApp = null;

        p_list = await db.getPatients(user._id, user.rolle).catch(error => console.error(error));
        if (p_list.length <= 0)
            return webData = {userData: user, t_list: null, p_list: null, selected: null};

        var selectedId = await db.getSelectedId(user).catch(error => console.error(error));
        if (selectedId == undefined)
            selectedId = 0;

        if (selectedId <= 0) {
            var selectedP = p_list[0].patient[0];
            selected = {patient: selectedP};
        } else {
            var tmpP = await db.getPatient(selectedId, user._id, user.rolle).catch(error => console.error(error));
            var selectedP = tmpP[0].patient[0];
            selected = {patient: selectedP};
        }

        var a_list = await db.getAerzte();

        t_list = await db.getAppointments(selected.patient._id, user.rolle).catch(error => console.error(error));
        if (t_list.length > 0) {
            selectedAppId = await db.getSelectedAppointment(user).catch(error => console.error(error));
            console.log(selectedAppId);
            if (selectedAppId != 0) {
                // get appointment with selected id
                selectedApp = await db.getAppointment(selectedAppId);
            } else {
                // get first element of list
                selectedApp = t_list[0];
            }
        }

        webData = {userData: user, t_list: t_list, p_list: p_list, a_list: a_list, selected: selected, selectedApp: selectedApp};
    }
    return webData;
}

function serveView(request, response, view) {

    var file, contentType;

    file = `./views/${view}.html`;
    contentType = `text/html`;

    fs.readFile(file, async (err, data) => {
        if (err) {
            console.log(err.message);
            return;
        }
        else {

            var source = data.toString();
            response.writeHead(303, { 'Content-Type': contentType } );
            response.write(source);
            response.end();
        }
    });
}

function serveHTML(request, response) {

    var file, contentType;

    var base = path.basename(request.url);
    base = base.split('?');
    base = base[0];

    file = `./views/${base}`;
    contentType = `text/html`;

    fs.readFile(file, async (err, data) => {
        if (err) {
            console.log(err.message);
            return;
        }
        else {

            var webData = await getRoleData(request).catch(error => console.error(error));

            var source = data.toString();
            var template = handlebars.compile(source);
            var result = template(webData);
           
            response.writeHead(200, { 'Content-Type': contentType });
            response.write(result);
            response.end();
        }
    });
}

// Serve functions
function serveCSS(request, response) {

    var file, contentType;

    file = `./css/${path.basename(request.url)}`;
    contentType = `text/css`;

    fs.readFile(file, (err, data) => {
        if (err) {
            console.log(err.message);
            return;
        }
        else {
            var source = data.toString();
            response.writeHead(200, { 'Content-Type': contentType });
            response.write(source);
            response.end();
        }
    });
}

function serveJS(request, response) {

    var file, contentType;

    file = `./js/${path.basename(request.url)}`;
    contentType = `text/javascript`;

    fs.readFile(file, (err, data) => {
        if (err) {
            console.log(err.message);
            return;
        }
        else {
            var source = data.toString();
            response.writeHead(200, { 'Content-Type': contentType });
            response.write(source);
            response.end();
        }
    });
}

function serveIndex(request, response) {

    var file, contentType;

    file = `index.html`;
    contentType = `text/html`;

    fs.readFile(file, (err, data) => {
        if (err) {
            console.log(err.message);
            return;
        }
        else {
            var source = data.toString();
            response.writeHead(response.statusCode, { 'Content-Type': contentType } );
            response.write(source);
            response.end();
        }
    });
}

function serveIMG(request, response) {

    var file, contentType;

    file = `./img/${path.basename(request.url)}`;
    var ext = path.extname(request.url);
    contentType = `image/${ext.split('.').pop()}`;

    fs.readFile(file, (err, data) => {
        if (err) {
            console.log(err.message);
            return;
        }
        else {
            response.writeHead(200, { 'Content-Type': contentType });
            response.write(data);
            response.end();
        }
    });
}

function start(port_nr){

    // Instanz eines http Servers erstellen. Server hoert 'port_nr' ab
    http.createServer(handleRequest).listen(port_nr);

    // Konsolenausgabe das Server laeuft mit 'port_nr'
    console.log('Server started, listening at port', port_nr);
}
      
start(8081);

// Funktion zum Einlesen von Daten bei einem POST request
function collectRequestData(request) {
    return new Promise((resolve, reject) => {

        const FORM_URLENCODED = 'application/x-www-form-urlencoded';
        if (request.headers['content-type'] === FORM_URLENCODED) {
    
            let body = '';
    
            request.on('data', chunk => {
                body += chunk.toString();
            });
    
            request.on('end', () => {
                resolve(parse(body));
            });
        }
    
        else {
            resolve(null);
        }
    });
}

// Funktion zum Generieren von Cookies
function generateCookie() {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < 20; i++ ) {
       result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

function parseCookies(request) {
    var list = {},
        rc = request.headers.cookie;

    rc && rc.split(';').forEach(function( cookie ) {
        var parts = cookie.split('=');
        list[parts.shift().trim()] = decodeURI(parts.join('='));
    });

    return list;
}

// db.insertOne("termin", {_id: "1", patient_id: "2", arzt_id: "3", behandlung: "Blutzucker", datum: "15.12.2020", uhrzeit: "14:00 Uhr"});
// db.insertOne("messwert", {_id: "1", user_id: "1", _type: "Blutzucker", datum: "11.04.2020", uhrzeit: "14:00 Uhr", werte: "101 mg/dol"});
// db.insertOne("angehoeriger", {patient_id: "2", angehoeriger_id: "1"});


 /*
cookie = request.headers.cookie;
var userId = await db.getUserId(cookie);
// TODO: wenn nicht vorhanden zurück zu startseite
var user = await db.getUser(userId);
var apmnts = await db.getAppointments(user._id, user.rolle);
var msrmnts = await db.getMeasurements(user._id);
var webData = {userData: user, termin: apmnts, messwert: msrmnts};
*/

// db.insertOne("messwert", {_id: 6, patient_id: 2, bezeichnung: "Blutzucker", werte: "101 mg/dol", datum: "11.04.2020", uhrzeit: "14:00 Uhr"});
// db.insertOne("termin", {_id: 5, patient_id: 2, arzt_id: 3, behandlung: "Blutzucker", datum: "15.12.2020", uhrzeit: "14:00 Uhr"});
// db.insertOne("angehoeriger", {patient_id: 7, angehoeriger_id: 6});

